<?php

class drubuntu_engine_dnsmasq implements drubuntu_engine {
  function install() {
    $hostname = drush_get_option('hostname', 'localhost');

    $cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ' . drush_get_option('dnsmasq-packages', DRUSH_DRUBUNTU_DNSMASQ_PACKAGES);
    drush_drubuntu_exec('Installing Dnsmasq packages.', $cmd, array(), 'Dnsmasq install successfull.', 'DRUBUNTU_DNSMASQ_INSTALL_FAILED', 'Problems were encountered installing Dnsmasq.');

    // Set up Dnsmasq for wildcard DNS, listen only on the "lo" interface.
    $dnsmasq_conf = '# Drubuntu configuration for dnsmasq.
# This is used to create a wildcard DNS for *.' . $hostname . ', so that multiple sites
# can be served without the disadvantages of subdirectory or /etc/hosts setups.

# These to configuration parameters ensure that dnsmasq plays nicely with VPNs
# registering their DNS servers when network-manager and resolvconf add them,
# and querying them before regular public DNS (still used as a fallback).
clear-on-reload
strict-order

# We listen, but only on the local interface.
interface=lo

# Add a wildcard DNS for *.' . $hostname . '.
address=/' . $hostname . '/127.0.0.1';
    drush_drubuntu_sudo_write('/etc/dnsmasq.d/drubuntu', $dnsmasq_conf);
    // Add to dhclient for users with dynamic IPs.
    $dhclient = file_get_contents('/etc/dhcp3/dhclient.conf');
    $dhclient = str_replace('#prepend domain-name-servers 127.0.0.1;', 'prepend domain-name-servers 127.0.0.1;', $dhclient);
    drush_drubuntu_sudo_write('/etc/dhcp3/dhclient.conf', $dhclient);
    // Set up resolv.conf for users with static IPs.
    $resolv_conf = file_get_contents('/etc/resolv.conf');
    // Ensure the local nameserver is above any others.
    $ns = "nameserver 127.0.0.1\n";
    $resolv_conf = str_replace($ns, '', $resolv_conf);
    $resolv_conf = preg_replace('/nameserver/', $ns . 'nameserver', $resolv_conf, 1, $count);
    if (!$count) {
      // No existing nameservers, so let's just add ours at the bottom of the file.
      $resolv_conf .= $ns; 
    }
    drush_drubuntu_sudo_write('/etc/resolv.conf', $resolv_conf);

    drush_drubuntu_exec('Restarting Dnsmasq.', 'sudo service dnsmasq restart', array(), 'Dnsmasq restart successfull.', 'DRUBUNTU_DNSMASQ_RESTART_FAILED', 'Problems were encountered restarting Dnsmasq.');
  }

  function uninstall($op) {
    // Remove from dhclient.
    $dhclient = file_get_contents('/etc/dhcp3/dhclient.conf');
    $dhclient = str_replace('prepend domain-name-servers 127.0.0.1;', '#prepend domain-name-servers 127.0.0.1;', $dhclient);
    drush_drubuntu_sudo_write('/etc/dhcp3/dhclient.conf', $dhclient);
    // Remove local nameserver from resolv.conf.
    $resolv_conf = file_get_contents('/etc/resolv.conf');
    $ns = "nameserver 127.0.0.1\n";
    $resolv_conf = str_replace($ns, '', $resolv_conf);
    drush_drubuntu_sudo_write('/etc/resolv.conf', $resolv_conf);
    if ($op == 'purge') {
        drush_drubuntu_exec('Cleaning up dnsmasq configuration.', 'sudo mv /etc/dnsmasq.d/drubuntu ' . drush_drubuntu_backup_dir(), array(), 'Files cleaned up.', 'DRUBUNTU_DNSMASQ_CLEANUP_FAILED', 'Problems were encountered cleaning up dnsmasq configuration.');
    }
  }
  function add_site() { print 'noop'; }
  function remove_site() { print 'noop'; }
}