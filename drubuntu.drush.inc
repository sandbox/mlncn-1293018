<?php

// Do not allow the program to be run as the root user. Ever.
$name = posix_getpwuid(posix_geteuid());
if ($name['name'] == 'root') {
  return drush_set_error('DRUBUNTU_IS_ROOT', dt('You are running the drubuntu script as the root user. Exiting'));
}

// Keep our engine definitions separate, for clarity.
include_once('engines.inc');

/**
 * Implementation of hook_drush_init().
 *
 * This sets the root and/or uri correctly, even when you are in a site
 * directory that is outside of the Drupal root.
 * We do this only if everything looks as we would expect, so we don't
 * interfere with the existing Drush detection logic.
 */
function drubuntu_drush_init() {
  $root = drush_get_option(array('r', 'root'));
  $uri = drush_get_option(array('l', 'uri'));
  if (empty($root) || empty($uri)) {
    $test_path = drush_site_path();
    if (!$test_path) {
      return;
    }
    $test_name = basename($test_path);
    $test_uri = $test_name . '.' . drush_get_option('hostname', 'localhost');
    $test_root = $_SERVER['HOME'] . '/' . drush_get_option('workspace', 'workspace') . '/' . substr($test_name, strrpos($test_name, '.') + 1);
  }
  if (empty($root) && is_dir($test_root) && file_exists($test_root . '/modules/system/system.module')) {    
    drush_set_option('r', $test_root);
  }
  if (empty($uri) && file_exists($test_root . '/sites/' . $test_uri . '/settings.php')) {
    drush_set_option('l', 'http://' . $test_uri);
  }
}

/**
 * Implementation of hook_drush_command().
 */
function drubuntu_drush_command() {
  $items = array();
  $globals = array(
    '--workspace' => 'Name of directory (within your home directory) to use as your project workspace. Default is "workspace".',
    '--hostname' => 'Hostname to use to refer to this system. Default is "localhost".',
  );

  $items['drubuntu-install'] = array(
    'description' => "Installs Drubuntu.",
    'options' => $globals + array(
      '--sandbox-platforms' => 'Names of platforms (comma separated) to set up initial sandboxes for. For example this could be "drupal-6.12,drupal-7.x,hyperlocalnews. Default is "drupal" (i.e. latest stable).',
      '--sandbox-name' => 'Name of site to generate within each platform. Default is "sandbox".',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
    'engines' => drush_drubuntu_tasks(),
  );
  $items['drubuntu-uninstall'] = $globals + array(
    'description' => "Unnstalls Drubuntu.",
    'options' => array(
      '--purge' => 'Remove settings and configuration (in addition to software packages).',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
  );
  $items['drubuntu-site-add'] = $globals + array(
    'description' => "Adds a site.",
    'arguments' => array(
      'name' => 'The name of the site.',
    ),
    'options' => array(
      '--install-platform' => 'Select a Drupal core to use for a new platform. E.g., drupal, drupal-6.12',
      '--profile' => 'Specify an installation profile to use in setting up the site. Must already be installed.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
    'aliases' => array('dsa'),
  );
  $items['drubuntu-site-remove'] = $globals + array(
    'description' => "Removes a site.",
    'arguments' => array(
      'name' => 'The name of the site.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH, // No bootstrap at all.
    'aliases' => array('dsr'),
  );

  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function drubuntu_drush_help($section) {
  switch ($section) {
    case 'drush:drubuntu-install':
      return dt("Installs Drubuntu");
  }
}

function drubuntu_task_invoke() {
  $args = func_get_args();
  $task = array_shift($args);
  $action = array_shift($args);
  if ($selections_csv = drush_get_option($task, FALSE)) {
    $selections = explode(',', $selections_csv);
  }
  else {
    // Use interactive engine selection, if more than one engine is available.
    $engines = drush_get_engines($task);
    foreach ($engines as $name => $engine) {
      $options[$name] = $engine['options']['--' . $task . '=' . $name];
    }
    $options_count = count($options);
    if ($options_count == 0) {
      return FALSE;
    }
    else if ($options_count == 1) {
      $selections = array_keys($options);
    }
    else {
      $selections = drush_choice_multiple($options, FALSE, 'Select ' . $task . ' options for installation');
    }
  }
  foreach ($selections as $selection) {
    drush_include_engine($task, $selection);
    $class = 'drubuntu_engine_' . $selection;
    $instance = new $class();
    call_user_func_array(array($instance, $action), $args);
  }
  return TRUE;
}

function drush_drubuntu_install() {
  drush_drubuntu_exec('Updating Ubuntu software index prior to installs.', 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update', array(), 'System update successfull.', 'DRUBUNTU_LAMP_UPDATE_FAILED', 'Problems were encountered updating your system prior to installation.');  
  $tasks = drush_drubuntu_tasks();
  foreach ($tasks as $task => $description) {
    if (!drubuntu_task_invoke($task, 'install')) {
      return drush_set_error('DRUBUNTU_TASK_NOT_FOUND', 'Could not find valid install task for ' . $task);
    }
  }
  drush_drubuntu_install_sandbox();
  if (drush_get_option('new-shell', FALSE)) {
    drush_print('User groups changed, starting new shell...');
    shell_exec('su --preserve-environment --command "$(which $SHELL) --login -i" $(whoami)');
    drush_print('NOTE: Drubuntu commands in this shell may not complete correctly.');
  }
  return TRUE;
}

function drush_drubuntu_exec($status, $cmd, $args, $successmessage, $failcode, $failmessage, $ignore_return = FALSE) {
  $password_note = '';
  if (strpos($cmd, 'sudo') !== FALSE) {
    $password_note = ' ' . dt('You may need to enter your password to continue.');
  }
  drush_print(dt($status) . $password_note);
  array_push($args, $cmd);
  if (!call_user_func_array('drush_shell_exec', $args) &&  $ignore_return == FALSE) {
    return drush_set_error($failcode, dt($failmessage) . dt("\nThe specific errors are below:\n!errors", array('!errors' => implode("\n", drush_shell_exec_output()))));
  }
  drush_log(dt($successmessage), 'success');
}

function drush_drubuntu_backup_dir() {
  $dir = $_SERVER['HOME'] . '/.drubuntu/backups/' . date('YmdHis', $_SERVER['REQUEST_TIME']);
  drush_mkdir($dir);  
  return $dir;
}

function drush_drubuntu_sudo_write($destination, $contents) {
  $filename_backup = trim(preg_replace('/[^A-Za-z0-9-_.]/', '_', $destination), '_');
  $filename_code = strtoupper($filename_backup);
  if (file_exists($destination)) {
    // Attempt to backup any existing file.
    $backup_destination = drush_drubuntu_backup_dir() . '/' . $filename_backup;
    // We read the file as sudo, but write it as the current user.
    drush_drubuntu_exec('Backing up file at ' . $destination . ' to ' . $backup_destination . '.', 'sudo cat ' . $destination . ' > ' . $backup_destination, array(), 'File at ' . $destination . ' was backed up successfully.', 'DRUBUNTU_FILE_BACKUP_FAIL_' . $filename_code, 'Unable to backup the file from ' . $destination . ' to ' . $backup_destination . '.');
  }
  $tmpname = tempnam('/tmp', 'drubuntu-write');
  file_put_contents($tmpname, $contents);
  // We copy, rather than move, to ensure correct file ownership.
  // TODO: make this dt()-able.
  drush_drubuntu_exec('Updating/creating file at ' . $destination . '.', 'sudo cp -f ' . $tmpname . ' ' . $destination, array(), 'File at ' . $destination . ' was placed successfully.', 'DRUBUNTU_FILE_PLACE_FAIL_' . $filename_code, 'Problems were encountered placing the file at ' . $destination . '.');
  drush_op('unlink', $tmpname);
}

function drush_drubuntu_install_sandbox() {
  $hostname = drush_get_option('hostname', 'localhost');

  // Add global drubuntu.settings.inc if it doesn't exist.
  if (!file_exists($_SERVER['HOME'] . '/.drubuntu/drubuntu.settings.inc')) {
    file_put_contents($_SERVER['HOME'] . '/.drubuntu/drubuntu.settings.inc', '<?php
/**
 * @file
 * Drubuntu global configuration file.
 *
 * Configuration added here will apply to all sites (including across all core
 * installs). This can be a useful place to apply generic development settings.
 * We hijack system_boot (which core system module implemented in core) as
 * a convenient place to inject values into the $conf array.
 * Some examples follow:
 */
function system_boot() {
  global $conf;
# $conf[\'preprocess_css\'] = FALSE;
# $conf[\'preprocess_js\'] = FALSE;
# $conf[\'securepages_enable\'] = FALSE;
# $conf[\'devel_rebuild_theme_registry\'] = TRUE;
}
');
  }

  $sandbox_platforms = explode(',', drush_get_option('sandbox-platforms', 'drupal'));
  foreach ($sandbox_platforms as $platform) {
    $data = array('install-platform' => trim($platform));
    drush_backend_invoke_args('drubuntu-site-add', array(drush_get_option('sandbox-name', 'sandbox') . '.' . $platform), $data);
  }
}

function drush_drubuntu_uninstall() {
  $op = 'remove';
  if (drush_get_option('purge', FALSE)) {
    $op = 'purge';
  }

  $tasks = drush_drubuntu_tasks();
  foreach ($tasks as $task => $description) {
    if (!drubuntu_task_invoke($task, 'uninstall', $op)) {
      return drush_set_error('DRUBUNTU_TASK_NOT_FOUND', 'Could not find valid install task for ' . $task);
    }
  }
  drush_drubuntu_uninstall_sandbox($op);
  return TRUE;
}


function drush_drubuntu_uninstall_sandbox($op) {
  if ($op == 'purge') {
    $sandbox_platforms = explode(',', drush_get_option('sandbox-platforms', 'drupal'));
    foreach ($sandbox_platforms as $platform) {
      drush_drubuntu_site_remove(drush_get_option('sandbox-name', 'sandbox') . '.' . $platform);
    }
    drush_drubuntu_exec('Cleaning up files.', 'sudo mv ' . $_SERVER['HOME'] . '/.drubuntu/drubuntu.settings.inc ' . drush_drubuntu_backup_dir(), array(), 'Files cleaned up.', 'DRUBUNTU_FILE_CLEANUP_FAILED', 'Problems were encountered cleaning up files.');
  }
}

function drush_drubuntu_db_query($query) {
  $pass = '';
  if (drush_get_option('db-password', '') !== '') {
    $pass = ' -p' . drush_get_option('db-password', '');
  }
  drush_shell_exec('mysql -u' . drush_get_option('db-user', 'root') . $pass . ' -h' . drush_get_option('db-host', 'localhost') . ' -P' . drush_get_option('db-port', '3306') . " -e'" . $query . "'");
}

function drush_drubuntu_get_platform($name, $create) {
  if (empty($name)) {
    return drush_set_error('DRUBUNTU_SITE_ADD_NO_NAME', dt('Invalid site name.'));
  }
  $workspace = drush_get_option('workspace', 'workspace');
  $site = array();
  // Allow users to use the full DNS name if they like.
  $name = str_replace('.' . drush_get_option('hostname', 'localhost'), '', $name);
  $platform = drush_locate_root();
  $site_name = $name;
  // If the user specified a platform, change to that directory first.
  $dot = strrpos($name, '.');
  if ($dot !== FALSE) {
    $platform = $_SERVER['HOME'] . '/'. $workspace .'/' . substr($name, $dot + 1);
    $site_name = substr($name, 0, $dot);
    if (is_dir($platform)) {
      drush_op('chdir', $platform);
    }
  }
  // If we don't have a valid platform, then create one.
  if (!drush_valid_drupal_root($platform)) {
    if (empty($platform)) {
      if (!$create) {
        return drush_set_error('DRUBUNTU_PLATFORM_NOT_FOUND', dt('Drupal core could not be found.'));
      }
      // We didn't have a site.platform syntax passed in, so we assume
      // that the name passed in is the desired platform name.
      $platform = $_SERVER['HOME'] . '/'. $workspace .'/' . $name;
      $site_name = 'default';
    }
    $data = array('destination' => $_SERVER['HOME'] . '/'. $workspace, 'drupal-project-rename' => basename($platform));
    $data += drush_get_context('cli');
    drush_backend_invoke_args('pm-download', array(drush_get_option('install-platform', 'drupal')), $data);
    if (!drush_valid_drupal_root($platform)) {
      return drush_set_error('DRUBUNTU_CORE_DOWNLOAD_FAILED', dt('Drupal core could not be downloaded successfully.'));
    }
  }

  $uri = $site_name . '.' . basename($platform) . '.' . drush_get_option('hostname', 'localhost');
  // Setup the environment for progressive bootstrap.
  drush_set_option('root', $platform);
  drush_set_option('uri', $uri);
  $_SERVER['SERVER_NAME'] = $uri;
  if (drush_bootstrap_validate(DRUSH_BOOTSTRAP_DRUPAL_ROOT)) {
    drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_ROOT);
    $site['root'] = $platform;
    $site['site'] = $site_name;
    $site['core'] = drush_get_context('DRUSH_DRUPAL_MAJOR_VERSION');
    $site['uri'] = $uri;
    $site['site_directory'] = drush_get_context('DRUSH_DRUPAL_ROOT') . '/sites/' . $uri;
    $site['workspace_directory'] = $_SERVER['HOME'] . '/'. $workspace .'/' . $site_name . '.' . basename($platform);
    $site['database'] = str_replace('.', '_', $uri);
    return $site;
  }
  return drush_set_error('DRUBUNTU_SITE_ADD_NO_ROOT', dt('Could not find a valid Drupal root to work on.'));
}

function drubuntu_eclipse_add_project($directories) {
  // We need to start Eclipse to add a project, which won't
  // work if it is already running, so we check here.
  if (drush_shell_exec('pgrep eclipse')) {
    if (!drush_confirm("Eclipse is currently running.\n- If you would like Drubuntu to add this site as an Eclipse project please exit Eclipse now then press 'y' to continue.\n- If you will add this project yourself, press 'n':")) {
      return;
    }
  }
  // The Eclipse CDT application we use to import projects doesn't like to import
  // projects into the workspace they exist in, so we reference them via a symlink.
  $staging = $_SERVER['HOME'] . '/.drubuntu/staging';
  if (!file_exists($staging)) {
    drush_op('symlink', $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace'), $staging);
  }
  foreach ($directories as $directory) {
    file_put_contents($directory . '/.project', '<?xml version="1.0" encoding="UTF-8"?>
<projectDescription>
<name>' . basename($directory) . '</name><comment></comment><projects></projects>
<buildSpec>
<buildCommand><name>org.eclipse.wst.validation.validationbuilder</name><arguments></arguments></buildCommand>
<buildCommand><name>org.eclipse.dltk.core.scriptbuilder</name><arguments></arguments></buildCommand>
</buildSpec>
<natures><nature>org.eclipse.php.core.PHPNature</nature></natures>
</projectDescription>');
    drush_shell_exec('eclipse -nosplash -data ' . $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace') . '  -application org.eclipse.cdt.managedbuilder.core.headlessbuild -import ' . $staging . '/' . basename($directory));
  }
}

function drush_drubuntu_site_add($name) {
  $site = drush_drubuntu_get_platform($name, TRUE);
  if (file_exists($site['site_directory'] . '/settings.php')) {
    drush_log('Site ' . $site['uri'] . ' appears to already exist');
    return;
  }
  drush_mkdir($site['workspace_directory']);
  drush_op('symlink', $site['workspace_directory'], $site['site_directory']);
  drush_mkdir($site['site_directory'] . '/files');
  $sudo = '';
  if (drush_get_option('new-shell', FALSE)) {
    // We are just installing, and this session doesn't have the www-data group
    // yet, so we need to sudo this.
    $sudo = 'sudo ';
  }
  drush_shell_exec($sudo . 'chgrp www-data ' . $site['site_directory'] . '/files');
  drush_op('chmod', $site['site_directory'] . '/files', 0770);

  $args = array(
    'db-url' => 'mysql://' . drush_get_option('db-user', 'root') . ':' . drush_get_option('db-password', '') . '@' . drush_get_option('db-host', 'localhost') . ':' . drush_get_option('db-port', '3306') . '/' . $site['database'],
    'sites-subdir' => $site['uri'],
  );
  // Pass options through.
  $args += drush_get_context('cli');
  // Pass profile through as an argument.
  array_unshift($args, drush_get_option('profile', 'default'));
  drush_backend_invoke('site-install', $args, 'GET', FALSE);
  if (drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_FULL)) {
    $account = user_load(1);
    $link = user_pass_reset_url($account);
    // Start browser in the background.
    shell_exec("sensible-browser '" . $link . "' > /dev/null 2>/dev/null &");
    // Add a project in Eclipse.
    drubuntu_eclipse_add_project(array($site['root'], $site['workspace_directory']));
    drush_log(dt('http://' . $site['uri'] . ' installed and project created successfully.'), 'success');
  }
  else {
    return drush_set_error('DRUBUNTU_DRUPAL_INSTALL_FAIL', dt('Drupal install was not successful.'));
  }
}

function drush_drubuntu_site_remove($name) {
  $site = drush_drubuntu_get_platform($name, FALSE);
  if (!file_exists($site['site_directory'] . '/settings.php')) {
    drush_log('Site ' . $site['uri'] . ' does not appear to exist');
    return;
  }

  // Back up and drop the database.
  drush_bootstrap(DRUSH_BOOTSTRAP_DRUPAL_CONFIGURATION);
  drush_set_option('result-file', drush_drubuntu_backup_dir() . '/' . $site['site'] . '.sql');
  drush_sql_dump_execute();
  drush_drubuntu_db_query('DROP DATABASE ' . $site['database']);

  // Remove site code.
  drush_shell_exec('mv ' . $site['site_directory'] . ' ' . drush_drubuntu_backup_dir());
  drush_shell_exec('mv ' . $site['workspace_directory'] . ' ' . drush_drubuntu_backup_dir());

  // If we just removed it's last site we can remove (backup) Drupal core also.
  $sites = $site['root'] . '/sites';
  if (is_dir($sites)) {
    $files = drush_scan_directory($sites, '/^settings\.php$/');
    if (empty($files)) {
      drush_shell_exec('mv ' . $site['root'] . ' ' . drush_drubuntu_backup_dir());
    }
  }
}
