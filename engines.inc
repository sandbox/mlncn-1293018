<?php

/**
 * MySQL default package list.
 */
define('DRUSH_DRUBUNTU_MYSQL_PACKAGES', 'mysql-server');

/**
 * Dnsmasq default package list.
 */
define('DRUSH_DRUBUNTU_DNSMASQ_PACKAGES', 'dnsmasq');

/**
 * Apache default package list.
 */
define('DRUSH_DRUBUNTU_APACHE_PACKAGES', 'apache2 apache2-mpm-prefork');

/**
 * Apache default module list.
 */
define('DRUSH_DRUBUNTU_APACHE_MODULES', 'vhost_alias rewrite');

/**
 * Postfix default package list.
 */
define('DRUSH_DRUBUNTU_POSTFIX_PACKAGES', 'postfix');

/**
 * PHP5 default package list.
 */
define('DRUSH_DRUBUNTU_PHP5_PACKAGES', 'curl libapache2-mod-php5 php5-cli php5-common php5-curl php5-dev php5-gd php5-mcrypt php5-mysql php5-sqlite php5-xdebug php5-xsl php-apc php-pear');

/**
 * PECL default package list.
 */
define('DRUSH_DRUBUNTU_PECL_PACKAGES', 'uploadprogress');

/**
 * Eclipse default package list.
 */
define('DRUSH_DRUBUNTU_ECLIPSE_PACKAGES', 'eclipse eclipse-pdt eclipse-cdt eclipse-plugin-cvs eclipse-subclipse');

/**
 * Webdev default package list.
 */
define('DRUSH_DRUBUNTU_WEBDEV_GENERAL_PACKAGES', 'cvs subversion svn-load git-core git-gui git-doc git-cola giggle gitg bzr meld mysql-query-browser mysql-admin phpmyadmin ghex siege gworldclock firefox epiphany-browser wine cabextract opera google-chrome-stable');

/**
 * Firefox addons default IDs (from addons.mozilla.org).
 * 60 - Web Developer Toolbar
 * 1843 - Firebug
 * 8370 - Drupal for Firebug
 */
define('DRUSH_DRUBUNTU_FIREFOX_ADDONS', '60,1843,8370');

/**
 * Default sets of development modules.
 */
define('DRUSH_DRUBUNTU_DEVELOPMENT_MODULES_P1', 'module_builder,trace,cache_disable,coder,masquerade,drupalforfirebug,watchbug,schema');
define('DRUSH_DRUBUNTU_DEVELOPMENT_MODULES_P2', 'cache_browser,reroute_email,firebug_lite,potx,simpletest,pqp,js_debug_helper,jsalter');
define('DRUSH_DRUBUNTU_DEVELOPMENT_MODULES_P3', 'retease,visualize_backtrace,security_scanner,devel_demo,coder_tough_love,simpletest_selenium');

function drush_drubuntu_tasks() {
  return array(
    'db' => 'Database backend(s) options.',
    'dns' => 'DNS wildcard aliasing options.',
    'www' => 'Web server options.',
    'mail' => 'Mail server local-only delivery options.',
    'php5' => 'PHP installation options.',
    'editor' => 'Editor/IDE options.',
    'devtools' => 'Development tool and utility options.',
  );
}

interface drubuntu_engine {
  function install();
  function uninstall($op);
  function add_site();
  function remove_site();
}

function drubuntu_drush_engine_db() {
  return array(
    'mysql' => array(
      'options' => array(
        '--db=mysql' => 'MySQL (default database).',
      ),
      'sub-options' => array(
        '--db=mysql' => array(
          '--mysql-packages' => 'List of packages to install with MySQL. Default is "' . DRUSH_DRUBUNTU_MYSQL_PACKAGES . '".',
          '--db-user' => 'Database user. Default is "root".',
          '--db-pass' => 'Database password. Default is no password.',
          '--db-host' => 'Database host. Default is "localhost".',
          '--db-port' => 'Database port. Default is "3306".',
        ),
      ),
    ),
  );
}

function drubuntu_drush_engine_dns() {
  return array(
    'dnsmasq' => array(
      'options' => array(
        '--dns=dnsmasq' => 'DNSMasq (default DNS masquerading).',
      ),
      'sub-options' => array(
        '--dns=dnsmasq' => array(
          '--dnsmasq-packages' => 'List of packages to install with Dnsmasq. Default is "' . DRUSH_DRUBUNTU_DNSMASQ_PACKAGES . '".',
        ),
      ),
    ),
  );
}

function drubuntu_drush_engine_www() {
  return array(
    'apache' => array(
      'options' => array(
        '--www=apache' => 'Apache (default web server).',
      ),
      'sub-options' => array(
        '--www=apache' => array(
          '--apache-packages' => 'List of packages to install with Apache. Default is "' . DRUSH_DRUBUNTU_APACHE_PACKAGES . '".',
          '--apache-modules' => 'List of Apache modules to enable. Default is "' . DRUSH_DRUBUNTU_APACHE_MODULES . '".',
        ),
      ),
    ),
  );
}

function drubuntu_drush_engine_mail() {
  return array(
    'postfix' => array(
      'options' => array(
        '--mail=postfix' => 'Postfix (default mail server).',
      ),
      'sub-options' => array(
        '--mail=postfix' => array(
          '--postfix-packages' => 'List of packages to install with Postfix. Default is "' . DRUSH_DRUBUNTU_POSTFIX_PACKAGES . '".',
        ),
      ),
    ),
  );
}

function drubuntu_drush_engine_php5() {
  return array(
    'packaged' => array(
      'options' => array(
        '--php5=packaged' => 'Ubuntu packaged PHP5 (default PHP).',
      ),
      'sub-options' => array(
        '--php5=packaged' => array(
          '--php-packages' => 'PHP packages to install. Default is "' . DRUSH_DRUBUNTU_PHP5_PACKAGES . '".',
          '--pecl-packages' => 'PECL packages to install. Default is "' . DRUSH_DRUBUNTU_PECL_PACKAGES . '".',
          '--php-memory-limit' => 'PHP memory_limit value to set (in MB). Defaults to "128".',
          '--php-php-upload-max-filesize' => 'PHP upload_max_filesize value to set (in MB). Defaults to "128".',
          '--php-post-max-size' => 'PHP post_max_size value to set (in MB). Defaults to "256".',
          '--php-error-reporting' => 'PHP error_reporting value to set. Defaults to "E_ALL".',
          '--php-auto-prepend' => 'PHP auto_prepend_file value to set. This file can be used to (for example) set $conf values across all workspace sites. Default is the .drubuntu/drubuntu.settings.inc file within your workspace.',
          '--php-xdebug-output-dir' => 'Directory to configure Xdebug to store traces etc. Defaults to xdebug directory inside your workspace.',
          '--php-xdebug-profiler-enable-trigger' => 'Value to set xdebug.profiler_enable_trigger to. If enabled this allows you to initiate profiling by adding a XDEBUG_PROFILE querystring parameter. Defaults to "1" (enabled).',
          '--php-xdebug-remote-enable' => 'Value to set xdebug.remote_enable to. If enabled this will allow debugging of pages loaded with your regular web browser. Defaults to "1" (enabled).',
          '--php-xdebug-max-nesting-level' => 'Value to set xdebug.max_nesting_level to. Defaults to "500".',
          '--php-apc-shm-size' => 'Size of APC shared memory allocation (in MB). Needs to be kept large enough to avoid fragmentation. Defaults to "128".',
        ),
      ),
    ),
  );
}

function drubuntu_drush_engine_editor() {
  return array(
    'eclipse' => array(
      'options' => array(
        '--editor=eclipse' => 'Eclipse with PDT (default editor).',
      ),
      'sub-options' => array(
        '--editor=eclipse' => array(
          '--eclipse-packages' => 'Eclipse packages to install. Default is ".' . DRUSH_DRUBUNTU_ECLIPSE_PACKAGES . '".',
          '--eclipse-xms' => 'Eclipse initial heap size (in MB). Default is "128".',
          '--eclipse-xmx' => 'Eclipse maximum heap size (in MB). Default is "512".',
        ),
      ),
    ),
  );
}

function drubuntu_drush_engine_devtools() {
  return array(
    'general' => array(
      'options' => array(
        '--devtools=general' => 'A general set of utilities and browsers (default).',
      ),
      'sub-options' => array(
        '--devtools=general' => array(
          '--webdev-general-packages' => 'General utility/browser packages to install. Default is "' . DRUSH_DRUBUNTU_WEBDEV_GENERAL_PACKAGES . '".',
          '--firefox-addons' => 'Firefox addons to install. Format is comma separated IDs (check addons.mozilla.org URLs for ID numbers). Default is "' . DRUSH_DRUBUNTU_FIREFOX_ADDONS . '".',      
        ),
      ),
    ),
  );
}
