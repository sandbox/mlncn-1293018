<?php

class drubuntu_engine_postfix implements drubuntu_engine {
  
  function install() {
    // Install Postfix (preselect local-only delivery to prevent accidentally sending out internet mail to real users on sandbox sites).
    drush_drubuntu_exec('Installing Postfix (for local-only mail delivery).',  'echo postfix postfix/main_mailer_type select Local only | sudo debconf-set-selections && sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ' . drush_get_option('postfix-packages', DRUSH_DRUBUNTU_POSTFIX_PACKAGES), array(), 'Postfix install successfull.', 'DRUBUNTU_POSTFIX_INSTALL_FAILED', 'Problems were encountered installing Postfix.');
  }

  function uninstall($op) { print 'noop'; }
  function add_site() { print 'noop'; }
  function remove_site() { print 'noop'; }
}