<?php

class drubuntu_engine_packaged implements drubuntu_engine {
  function install() {
    $cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install ' . drush_get_option('php-packages', DRUSH_DRUBUNTU_PHP5_PACKAGES);
    drush_drubuntu_exec('Installing PHP packages.', $cmd, array(), 'PHP install successfull.', 'DRUBUNTU_PHP_PACKAGED_INSTALL_FAILED', 'Problems were encountered installing PHP.');

    // Set up PHP configuration.
    $xdebug_directory = drush_get_option('php-xdebug-output-dir', $_SERVER['HOME'] . '/'. drush_get_option('workspace', 'workspace') .'/xdebug');
    $global_include = drush_get_option('php-auto-prepend-file', $_SERVER['HOME'] . '/.drubuntu/drubuntu.settings.inc');
    if (!empty($global_include)) {
      $global_include = "\n" . 'auto_prepend_file=' . $global_include;
    }
    drush_drubuntu_sudo_write('/etc/php5/conf.d/drubuntu.ini', '
; Below configuration added by Drubuntu.
;
; This file configures PHP and associated modules for Drupal development.

; PHP settings
memory_limit=' . drush_get_option('php-memory-limit', '128') . 'M
upload_max_filesize=' . drush_get_option('php-upload-max-filesize', '128') . 'M
post_max_size=' . drush_get_option('php-post-max-size', '256') . 'M
error_reporting=' . drush_get_option('php-error-reporting', 'E_ALL') .
$global_include . '

; Xdebug settings
; Set directories, and allow enabling via URL parameters.
xdebug.profiler_output_dir=' . $xdebug_directory . '
xdebug.profiler_enable_trigger=' . drush_get_option('php-xdebug-profiler-enable-trigger', '1') . '
xdebug.trace_output_dir=' . $xdebug_directory . '
xdebug.remote_enable=' . drush_get_option('php-xdebug-remote-enable', '1') . '
xdebug.max_nesting_level=' . drush_get_option('php-xdebug-max-nesting-level', '500') . '

; APC settings
; Increase cache size, to prevent fragmentation.
apc.shm_size=' . drush_get_option('php-apc-shm-size', '128') . '
  ');
    drush_mkdir($xdebug_directory);
    // This shell session will not be a member of the www-data group, so we sudo here.
    drush_shell_exec('sudo chgrp www-data ' . $xdebug_directory);
  
    // Install PECL modules.
    drush_drubuntu_exec('Installing PECL packages.', 'sudo pecl install -sf ' . drush_get_option('pecl_packages', DRUSH_DRUBUNTU_PECL_PACKAGES), array(), 'PECL install successfull.', 'DRUBUNTU_PECL_INSTALL_FAILED', 'Problems were encountered installing software through PECL.');
    drush_drubuntu_sudo_write('/etc/php5/conf.d/uploadprogress.ini', 'extension=uploadprogress.so');
  }

  function uninstall($op) { 
    drush_drubuntu_exec('Uninstalling PECL packages.', 'sudo pecl uninstall ' . drush_get_option('pecl-packages', DRUSH_DRUBUNTU_PECL_PACKAGES), array(), 'PECL uninstall successfull.', 'DRUBUNTU_PECL_UNINSTALL_FAILED', 'Problems were encountered uninstalling software through PECL.');
    $cmd = 'sudo DEBIAN_FRONTEND=noninteractive apt-get -yq ' . $op . ' ' . drush_get_option('php-packages', DRUSH_DRUBUNTU_PHP5_PACKAGES);
    drush_drubuntu_exec('Uninstalling PHP packages.', $cmd, array(), 'PHP uninstall successfull.', 'DRUBUNTU_PHP_PACKAGED_UNINSTALL_FAILED', 'Problems were encountered uninstalling PHP.');
    if ($op == 'purge') {
      $workspace = $_SERVER['HOME'] . '/' . drush_get_option('workspace', 'workspace');
      $cmd = 'sudo mv ' .
        $workspace . '/xdebug ' .
        '/etc/php5 ' . drush_drubuntu_backup_dir();
      drush_drubuntu_exec('Cleaning up files.', $cmd, array(), 'Files cleaned up.', 'DRUBUNTU_FILE_CLEANUP_FAILED', 'Problems were encountered cleaning up files.');
    }
  }
  function add_site() { print 'noop'; }
  function remove_site() { print 'noop'; }
}
